import numpy as np
import datetime
import time
import glob
import sys
import os


# Sugar coating binary loads of .dat files with a single column
def load_binary(filename):
    f = open(filename, 'rb')
    x = np.fromfile(f, dtype='float32')
    f.close()
    return x


# Sugar coating binary writes of .dat files with a single column
def save_binary(filename, x):
    f = open(filename, 'wb')
    x.astype('float32').tofile(f)
    f.close()


def get_files(folder, key='*'):
    files = glob.glob(os.path.join(folder, key))
    files = sorted([f.replace('\\', '/') for f in files])
    return files


# Quick way of getting PSD for visualization of the frequency domain of a signal
def get_psd(x, fs=2000):
    N = len(x)
    T = 1. / fs
    fx = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
    fy = np.fft.fft(x)[:len(x) // 2]
    fy = np.real(fy * np.conj(fy)) / (fs * len(x))
    fy[1:-1] *= 2
    return fx, fy


# Command line sugar to visualize progress of slow tasks
# i = iteration number of n iterations
# persist determines if the bar will stay after completion
# p_steps how many steps the bar will show
# post_fix adds extra string after the progress bar
def command_progress(i, n, persist=False, p_steps=40, post_fix='', eta=False):
    if i <= 1 or command_progress.cmd_time is None:
        command_progress.cmd_time = time.time()
    progress = (i * p_steps) // n
    prog_cof = (float(i) / float(n))
    prog_str = '[' + '-' * progress + ' ' * (p_steps - progress) + '] %.2f%%' % (prog_cof * 100.0)
    if post_fix != '':
        prog_str += ' (%s)' % post_fix
    if eta and i > 1 and command_progress.cmd_time is not None:
        dt = (time.time() - command_progress.cmd_time) / prog_cof
        eta_time = (datetime.datetime.now() + datetime.timedelta(seconds=int(dt))).strftime('%Y.%m.%d %H:%M:%S')
        prog_str += ' ETA: %.2f seconds - %s' % (dt, eta_time)
    sys.stdout.write('\r%s ' % prog_str)
    sys.stdout.flush()
    if i == n and not persist:
        command_progress.cmd_time = None
        sys.stdout.write('\r')
        sys.stdout.flush()
command_progress.cmd_time = None


def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising', kpsh=False, valley=False):
    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan - 1, indnan + 1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size - 1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind] - x[ind - 1], x[ind] - x[ind + 1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                              & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])
    return ind

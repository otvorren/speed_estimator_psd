# Stuff
from scipy.ndimage.filters import gaussian_filter1d
import matplotlib.pyplot as plt
from scipy.signal import welch
import tensorflow as tf
import pandas as pd
import numpy as np
import random
import utils
import os


class Dataset:
    def __init__(self, **kwargs):
        self.data = {}

        mode = kwargs.pop('mode', 'training')
        data_x = np.load('Cache/gen_ocvia_x.npy')
        data_y = np.load('Cache/gen_ocvia_y.npy')
        data_x = np.concatenate((data_x, np.load('Cache/gen_tvk_x.npy')))
        data_y = np.concatenate((data_y, np.load('Cache/gen_tvk_y.npy')))
        # if mode == 'training':
        #     data_x = np.load('Cache/gen_ocvia_x.npy')
        #     data_y = np.load('Cache/gen_ocvia_y.npy')
        #     data_x = np.concatenate((data_x, np.load('Cache/gen_tvk_x.npy')))
        #     data_y = np.concatenate((data_y, np.load('Cache/gen_tvk_y.npy')))
        # elif mode == 'validate':
        #     data_x = np.load('Cache/gen_db_x.npy')
        #     data_y = np.load('Cache/gen_db_y.npy')
        # else:
        #     raise ValueError

        temp = {}
        for x, y in zip(data_x, data_y):
            if y[4] == 'CARGO':
                data_key = '%s-%s-%s' % (y[1], y[2], y[4])
            else:
                data_key = '%s-%s-%s-%s' % (y[1], y[2], y[3], y[4])
            speed = min(max((float(y[0]) / 160.0) - 1.0, -1.0), 1.0)
            if data_key not in temp:
                temp[data_key] = []
            temp[data_key].append([x, speed])

        keys = temp.keys()
        for key in keys:
            print('%s - %d' % (key, len(temp[key])))
            if len(temp[key]) > 5:
                n = len(temp[key]) // 2
                if mode == 'training':
                    self.data[key] = temp[key][:n]
                else:
                    self.data[key] = temp[key][n:]
        self.labels = list(self.data.keys())

    def get_mini_dataset(self, batch_size, repetitions, shots, num_classes, split=False):
        temp_labels = np.zeros(shape=(num_classes * shots))
        temp_traces = np.zeros(shape=(num_classes * shots, 256, 1))
        if split:
            test_labels = np.zeros(shape=(num_classes))
            test_traces = np.zeros(shape=(num_classes, 256, 1))

        label_subset = random.choices(self.labels, k=num_classes)
        for class_idx, class_obj in enumerate(label_subset):
            if split:
                traces_to_split = random.choices(self.data[label_subset[class_idx]], k=shots + 1)
                test_traces[class_idx] = traces_to_split[-1][0]
                test_labels[class_idx] = traces_to_split[-1][1]
                temp_traces[class_idx * shots:(class_idx + 1) * shots] = [v[0] for v in traces_to_split[:-1]]
                temp_labels[class_idx * shots:(class_idx + 1) * shots] = [v[1] for v in traces_to_split[:-1]]
            else:
                traces = random.choices(self.data[label_subset[class_idx]], k=shots)
                temp_traces[class_idx * shots:(class_idx + 1) * shots] = [v[0] for v in traces]
                temp_labels[class_idx * shots:(class_idx + 1) * shots] = [v[1] for v in traces]

        dataset = tf.data.Dataset.from_tensor_slices((temp_traces.astype(np.float32), temp_labels.astype(np.float32)))
        dataset = dataset.shuffle(100).batch(batch_size).repeat(repetitions)
        if split:
            return dataset, test_traces, test_labels
        return dataset

    def get_full_dataset(self, split=False):
        traces, labels = [], []
        keys = self.labels
        for key in keys:
            n = len(self.data[key]) // 2
            if split:
                data = self.data[key][n:]
            else:
                data = self.data[key][:n]
            for row in data:
                traces.append(row[0])
                labels.append(row[1])
        traces = np.array(traces).reshape((-1, 256, 1))
        labels = np.array(labels).reshape((-1, 1))
        # dataset = tf.data.Dataset.from_tensor_slices((traces.astype(np.float32), labels.astype(np.float32)))
        return traces, labels


def extract_wnd_psd(x):
    psd = utils.get_psd(x, fs=2000)[1]
    psd = np.sqrt(psd * 100.0)
    return psd


def extract_wnd_feature(x, spd, x_train, y_train, input_length, model_length):
    for i in range(0, len(x) - input_length * 2, input_length):
        psd = extract_wnd_psd(x[i:i + input_length * 2])[:model_length]
        x_train.append(psd)
        y_train.append(spd)
    return x_train, y_train


def extract_welch_peaks(x, spd, x_train, y_train, input_length, model_length):
    x_max = max(sorted(x)[int(float(len(x)) * 0.99)], 2.0)
    x = x / x_max
    w = welch(x, fs=2000, nperseg=input_length * 2, noverlap=4)[1][:-1][:model_length]
    p = utils.detect_peaks(w, mpd=10)
    psd = np.zeros(model_length)
    psd[p] = 1.0
    psd = gaussian_filter1d(psd, 2)
    psd /= np.max(psd)
    x_train.append(psd)
    y_train.append(spd)
    return x_train, y_train


def extract_welch_psd(x, spd, x_train, y_train, input_length, model_length):
    psd = welch(x, fs=2000, nperseg=input_length * 2, noverlap=4)[1][:-1][:model_length]
    psd = np.sqrt(psd) * 100.0
    psd /= sorted(psd)[int(0.99 * model_length)]
    x_train.append(psd)
    y_train.append(spd)
    return x_train, y_train


def extract_feature(x, spd, x_train, y_train, input_length, model_length=None, feature_name='wnd_psd'):
    model_length = input_length if model_length is None else model_length
    if len(x) < input_length:
        return x_train, y_train
    if feature_name.lower() == 'wnd_psd':
        x_train, y_train = extract_wnd_feature(x, spd, x_train, y_train, input_length, model_length)
    elif feature_name.lower() == 'welch_peak':
        x_train, y_train = extract_welch_peaks(x, spd, x_train, y_train, input_length, model_length)
    elif feature_name.lower() == 'welch_psd':
        x_train, y_train = extract_welch_psd(x, spd, x_train, y_train, input_length, model_length)
    else:
        raise ValueError
    return x_train, y_train


# Load the partial trace dataset from Oc'via assembled on 2020-05-18
def load_ocvia(debug=False):
    dataset_path = 'D:/Data/20200518 - Ocvia Speed Phase 2/20200518 - Ocvia Speed Phase 2'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]

    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]

    rows = []
    for label in labels:
        x_file = utils.get_files('%s/data' % dataset_path, '%s*' % label[0][:24])
        if len(x_file) < 1:
            continue
        x_file = x_file[0]
        if not os.path.isfile(x_file):
            continue
        rows.append([x_file, float(label[1])])
    return rows


# Load the full trace dataset from Oc'via sensors assembled on 2020-01-31
def load_ocvia_alt(debug):
    dataset_path = 'D:/Data/20200131_Ocvia_Synth'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, label[0]), float(label[6])] for label in labels]
    return rows


# Load general speed truth dataset from DB assembled on 2019-06-13
def load_gen_speed(debug):
    dataset_path = 'D:/Data/Speed_Truth_v01'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[8]), l[2], l[3], l[7]] for l in labels]
    return rows


# Load from Synth And Train Speed dataset from DB and TVK assembled 2019-08-04
def load_sats(debug):
    dataset_path = 'D:/Data/SATS Dataset'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, label[0]), float(label[4])] for label in labels]
    return rows


def load_gen_db(debug, **kwargs):
    subsampling = kwargs.pop('subsampling', False)
    sub_asset = kwargs.pop('sub_asset', None)

    dataset_path = 'D:/Data/20200525 - General Speed/db'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    elif subsampling:
        if sub_asset is not None:
            buckets = {}
            for line in labels:
                key = '%s+%s' % (line[2], line[3])
                if key not in buckets:
                    buckets[key] = []
                buckets[key].append(line)
            # buckets = {'%s+%s' % (line[2], line[3]): line for line in labels}
            keys = list(buckets.keys())
            np.random.shuffle(keys)
            keys = keys[:int(float(len(keys)) * sub_asset)]
            labels = []
            for key in keys:
                np.random.shuffle(buckets[key])
                labels = list(labels) + list((buckets[key][:20]))
    # rows = [['%s/data/%s' % (dataset_path, label[0]), float(label[5])] for label in labels]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[5]), l[2], l[3], l[7], l[8]] for l in labels]
    return rows


def load_gen_ocvia(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/ocvia'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[5]), l[2], l[3], l[7], l[8]] for l in labels]
    return rows


def load_gen_jre(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/jre'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[5]), l[2], l[3], l[7], l[8]] for l in labels]
    return rows


def load_gen_sncf(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/sncf'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[5]), l[2], l[3], l[7], l[8]] for l in labels]
    return rows


def load_gen_tvk(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/tvk'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), float(l[5]), l[2], l[3], l[7], l[8]] for l in labels]
    return rows


def load_gen_bn(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/bn'
    df = pd.read_csv('%s/labels.csv' % dataset_path)
    x_files = df['local_filename'].values
    spd_lbl = df['corrected_speed'].values
    swn = df['asset'].values
    psn = df['measurement_location'].values
    tts = df['train_type'].values
    cls = 'PASSENGER'
    labels = [[x_file, spd, sw, ps, tt, cls] for x_file, spd, sw, ps, tt in zip(x_files, spd_lbl, swn, psn, tts) if spd < 180.0]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), l[1], l[2], l[3], l[4], l[5]] for l in labels]
    return rows


def load_gen_fgc(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/fgc'
    df = pd.read_parquet('%s/final_speed_df.parquet' % dataset_path)
    x_files = df['local_filename'].values
    spd_lbl = df['final_speed'].values
    swn = df['asset'].values
    psn = df['measurement_location'].values
    labels = [[x_file, spd, sw, ps, '112', 'PASSENGER'] for x_file, spd, sw, ps in zip(x_files, spd_lbl, swn, psn)]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, l[0]), l[1], l[2], l[3], l[4], l[5]] for l in labels]
    return rows


def load_mini_sncf(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/mini_sncf'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, label[0]), float(label[1])] for label in labels]
    return rows


def load_tgv_synth(debug):
    dataset_path = 'D:/Data/20200525 - General Speed/tgv_synth'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]
    rows = [['%s/data/%s' % (dataset_path, label[0]), float(label[1])] for label in labels]
    return rows


def get_data_rows(dataset, debug=False, **kwargs):
    dataset = dataset.lower()
    if dataset == 'ocvia':
        print('Loading Data From Ocvia Dataset')
        rows = load_ocvia(debug)
    elif dataset == 'ocvia_alt':
        print('Loading Data From Alternative Ocvia Dataset')
        rows = load_ocvia_alt(debug)
    elif dataset == 'gen_speed':
        print('Loading Data From DB Dataset')
        rows = load_gen_speed(debug)
    elif dataset == 'sats_data':
        print('Loading Data From SATS Dataset')
        rows = load_sats(debug)
    elif dataset == 'gen_bn':
        print('Loading Data From General Speed BN')
        rows = load_gen_bn(debug)
    elif dataset == 'gen_db':
        print('Loading Data From General Speed DB')
        rows = load_gen_db(debug, **kwargs)
    elif dataset == 'gen_fgc':
        print('Loading Data From General Speed FGC')
        rows = load_gen_fgc(debug)
    elif dataset == 'gen_jre':
        print('Loading Data From General Speed JRE')
        rows = load_gen_jre(debug)
    elif dataset == 'gen_ocvia':
        print('Loading Data From General Speed Ocvia')
        rows = load_gen_ocvia(debug)
    elif dataset == 'gen_sncf':
        print('Loading Data From General Speed SNCF')
        rows = load_gen_sncf(debug)
    elif dataset == 'gen_tvk':
        print('Loading Data From General Speed TVK')
        rows = load_gen_tvk(debug)
    elif dataset == 'mini_sncf':
        print('Loading Data From Mini SNCF')
        rows = load_mini_sncf(debug)
    elif dataset == 'tgv_synth':
        print('Loading Data From TGV Synth')
        rows = load_tgv_synth(debug)
    else:
        raise ValueError
    return rows


def cache_data(dataset, **kwargs):
    input_length = kwargs.pop('input_length', 1024)
    model_length = kwargs.pop('model_length', None)
    data_type = kwargs.pop('data_type', 'training')
    feature = kwargs.pop('feature_name', 'wnd_psd')
    debug = kwargs.pop('debug', False)

    # Get file paths and speed labels from selected source
    rows = get_data_rows(dataset, debug, **kwargs)
    props = []

    count = 0
    x_train, y_train = [], []
    for row in rows:
        count += 1
        utils.command_progress(count, len(rows), post_fix='Loading Training Data')

        x_file, speed, switch, position, ttype, ttcls = row
        if os.path.getsize(x_file) / 4 < input_length * 2:
            continue
        x = utils.load_binary(x_file)
        if dataset not in ['ocvia', 'gen_ocvia', 'mini_sncf', 'tgv_synth']:
            x = x[400:-4000]
        if dataset == 'gen_fgc':
            x = x[:int(len(x) // 1000) * 1000]
            x = np.mean(x.reshape(-1, 10), axis=1)
            x = x[400:-4000]
        if len(x) < input_length * 2:
            continue
        props.append([speed, switch, position, ttype, ttcls])
        x_train, y_train = extract_feature(x, speed, x_train, y_train, input_length, model_length, feature)

    x_train = np.array(x_train).reshape((-1, model_length, 1))
    # cache_file = '%s_%s_%s_%d-%s' % (dataset, data_type, input_length, model_length, feature)
    np.save('Cache/%s_x.npy' % dataset, x_train)
    np.save('Cache/%s_y.npy' % dataset, np.array(props))


def load_cache(dataset, **kwargs):
    xc, yc = 0.7, 0.2

    random.seed(1337)
    np.random.seed(1337)
    data_x = np.load('Cache/%s_x.npy' % dataset)
    data_y = np.load('Cache/%s_y.npy' % dataset)

    # Divide into bins
    data = {}
    for x, y in zip(data_x, data_y):
        key = '%s-%s' % (y[1], y[2])
        if key not in data:
            data[key] = []
        data[key].append([x, y])

    # Extract in accordance to splits
    t_x, t_y, t_z = [], [], []
    keys = sorted(data)
    for key in keys:
        n = float(len(data[key]))
        np.random.shuffle(data[key])
        t_x += data[key][:int(xc * n)]
        t_z += data[key][int((xc + yc) * n):]
        t_y += data[key][int(xc * n):int((xc + yc) * n)]

    xy = [['', v[0], min(max((float(v[1][0]) / 160.0) - 1.0, -1.0), 1.0)] for v in t_x]
    uv = [['', v[0], min(max((float(v[1][0]) / 160.0) - 1.0, -1.0), 1.0), v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_y]
    wz = [['', np.array(v[0]).reshape((-1, 256, 1)), float(v[1][0]), v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_z]

    # xy = [['', v[0], v[1][0]] for v in t_x]
    # uv = [['', v[0], v[1][0], v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_y]
    # wz = [['', np.array(v[0]).reshape((-1, 256, 1)), v[1][0], v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_z]
    return xy, uv, wz


def load_binned_cache(dataset, **kwargs):
    random.seed(1337)
    np.random.seed(1337)
    data_x = np.load('Cache/%s_x.npy' % dataset)
    data_y = np.load('Cache/%s_y.npy' % dataset)

    # Divide into bins
    data = {}
    for x, y in zip(data_x, data_y):
        key = '%s-%s' % (y[1], y[2])
        if key not in data:
            data[key] = []
        data[key].append([x, y])

    for key in data:
        np.random.shuffle(data[key])
    return data


def load_bin_index(data, index, **kwargs):
    xc, yc = 0.6, 0.2

    # Extract in accordance to splits
    t_x, t_y, t_z = [], [], []
    keys = sorted(data)
    for key in keys:
        n = float(len(data[key]))
        temp = data[key][int(n * float(index) * 0.2):] + data[key][:int(n * float(index) * 0.2)]
        t_x += temp[:int(xc * n)]
        t_z += temp[int((xc + yc) * n):]
        t_y += temp[int(xc * n):int((xc + yc) * n)]

    xy = [['', v[0], min(max((float(v[1][0]) / 160.) - 1., -1.), 1.)] for v in t_x]
    uv = [['', v[0], min(max((float(v[1][0]) / 160.) - 1., -1.), 1.), v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_y]
    wz = [['', np.array(v[0]).reshape((-1, 256, 1)), float(v[1][0]), v[1][1], v[1][2], v[1][3], v[1][4]] for v in t_z]
    return xy, uv, wz


# TODO: Add support for selection of multiple datasets
# TODO: Add support for cache_file generation reflecting dataset and feature extraction parameters
def load_training_data(dataset, **kwargs):
    input_length = kwargs.pop('input_length', 1024)
    model_length = kwargs.pop('model_length', None)
    data_type = kwargs.pop('data_type', 'training')
    feature = kwargs.pop('feature_name', 'wnd_psd')
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)
    cache_file = ''

    if data_type not in ['training', 'validation']:
        raise ValueError

    if cache:
        cache_file = '%s_%s_%s_%d-%s' % (dataset, data_type, input_length, model_length, feature)
        if data_type == 'training':
            if os.path.isfile('Cache/%s_x.dat' % cache_file):
                x_train = utils.load_binary('Cache/%s_x.dat' % cache_file).reshape((-1, model_length, 1))
                y_train = utils.load_binary('Cache/%s_y.dat' % cache_file)
                data = [[x_t, y_t] for x_t, y_t in zip(x_train, y_train)]
                return data
        else:
            pass

    # Get file paths and speed labels from selected source
    rows = get_data_rows(dataset, debug, **kwargs)

    data_type = data_type.lower()
    if data_type == 'training':
        count = 0
        x_train, y_train = [], []
        for row in rows:
            count += 1
            utils.command_progress(count, len(rows), post_fix='Loading Training Data')

            # x_file, speed = row
            x_file, speed, switch, position, ttype, ttcls = row
            if os.path.getsize(x_file) / 4 < input_length * 2:
                continue
            x = utils.load_binary(x_file)
            if dataset not in ['ocvia', 'gen_ocvia', 'mini_sncf', 'tgv_synth']:
                x = x[400:-4000]
            if len(x) < input_length * 2:
                continue

            # speed /= 10.0
            speed = min(max((speed / 160.0) - 1.0, -1.0), 1.0)
            x_train, y_train = extract_feature(x, speed, x_train, y_train, input_length, model_length, feature)
        x_train = np.array(x_train).reshape((-1, model_length, 1))
        y_train = np.array(y_train)
        data = [[x_t, y_t] for x_t, y_t in zip(x_train, y_train)]
    else:
        data = []
        count = 0
        for row in rows:
            count += 1
            utils.command_progress(count, len(rows), post_fix='Loading Validation Data')
            # x_file, speed = row
            x_file, speed, switch, position, ttype, ttcls = row
            if os.path.getsize(x_file) / 4 < input_length * 2:
                continue
            x = utils.load_binary(x_file)
            if dataset not in ['ocvia', 'gen_ocvia', 'mini_sncf', 'tgv_synth']:
                x = x[400:-4000]
            if len(x) < input_length * 2:
                continue

            # speed /= 10.0
            speed = min(max((speed / 160.0) - 1.0, -1.0), 1.0)
            x_train, y_train = [], []
            x_train, y_train = extract_feature(x, speed, x_train, y_train, input_length, model_length, feature)
            x_train = np.array(x_train).reshape((-1, model_length, 1))
            y_train = np.array(y_train)
            data.append([x, x_train, y_train])

    # Cache processed data for quicker re-use later
    if cache:
        if data_type == 'training':
            x_train = np.array([value[0] for value in data])
            y_train = np.array([value[1] for value in data])
            utils.save_binary('Cache/%s_x.dat' % cache_file, x_train.reshape((-1)))
            utils.save_binary('Cache/%s_y.dat' % cache_file, y_train)
        else:
            pass

    return data


def load_data(dataset, **kwargs):
    input_length = kwargs.pop('input_length', 1024)
    model_length = kwargs.pop('model_length', None)
    feature = kwargs.pop('feature_name', 'wnd_psd')
    debug = kwargs.pop('debug', False)

    # Get file paths and speed labels from selected source
    rows = get_data_rows(dataset, debug)

    data = []
    count = 0
    for row in rows:
        count += 1
        utils.command_progress(count, len(rows), post_fix='Loading Validation Data')
        x_file, speed, switch, position, ttype, ttcls = row
        if os.path.getsize(x_file) / 4 < input_length * 2:
            continue
        x = utils.load_binary(x_file)
        if dataset not in ['ocvia', 'gen_ocvia', 'mini_sncf', 'tgv_synth']:
            x = x[400:-4000]
        if len(x) < input_length * 2:
            continue

        # speed /= 10.0
        speed = min(max((speed / 160.0) - 1.0, -1.0), 1.0)
        x_train, y_train = [], []
        x_train, y_train = extract_feature(x, speed, x_train, y_train, input_length, model_length, feature)
        x_train = np.array(x_train).reshape((-1, model_length, 1))
        y_train = np.array(y_train)
        data.append([x_file, x_train, y_train, switch, position, ttype, ttcls])
    return data


def store_results(dataset, data, values):
    dataset_path = 'D:/Data/20200525 - General Speed/%s' % (dataset.replace('gen_', ''))
    if dataset == 'gen_bn':
        df = pd.read_csv('%s/labels.csv' % dataset_path)
        v_a = df['local_filename'].values
        v_b = df['asset'].values
        v_c = df['measurement_location'].values
        v_d = df['corrected_speed'].values
        labels = {a: [a, '', b, c, '', d] for a, b, c, d in zip(v_a, v_b, v_c, v_d)}
    else:
        with open('%s/labels.csv' % dataset_path, 'r') as fp:
            labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
            labels = {line[0]: line for line in labels}

    errors = {}
    ls, ps = [], []
    output_str = 'filename,speed\n'
    for i in range(len(data)):
        x_file = data[i][0]
        speed = values[i]
        label = labels[x_file.split('/')[-1]]
        switch = label[2]
        position = label[3]
        # if label[8] != 'CARGO':
        #     continue
        ref_speed = float(label[5])
        err = speed - ref_speed
        rel = speed / ref_speed - 1.0
        ls.append(ref_speed)
        ps.append(speed)

        key = '%s+%s' % (switch, position)
        if key not in errors:
            errors[key] = []
        errors[key].append([err, rel])
        output_str += '%s,%.3f\n' % (x_file.split('/')[-1], speed)

    stats_str = ''
    fe, fr = [], []
    keys = sorted(errors)
    for key in keys:
        an = key.split('+')[0]
        pn = key.split('+')[1]
        e = [v[0] for v in errors[key]]
        r = [v[1] for v in errors[key]]
        fe = list(fe) + list(e)
        fr = list(fr) + list(r)
        std = float(np.std(e))
        mbe = float(np.mean(e))
        mae = float(np.mean(np.abs(e)))
        rle = np.mean(np.abs(r)) * 100.0
        e10 = np.mean([1.0 if abs(ri) >= 0.1 else 0.0 for ri in r]) * 100.0
        sss = '%16s - %20s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%' % (an, pn, mae, mbe, std, rle, e10)
        print(sss)
        stats_str += sss + '\n'
    std = float(np.std(fe))
    mbe = float(np.mean(fe))
    mae = float(np.mean(np.abs(fe)))
    rle = np.mean(np.abs(fr)) * 100.0
    e10 = np.mean([1.0 if abs(ri) >= 0.1 else 0.0 for ri in fr]) * 100.0
    print('%39s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%' % ('Overall', mae, mbe, std, rle, e10))
    stats_str += '%39s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%\n' % ('Overall', mae, mbe, std, rle, e10)

    t_bins = np.linspace(-0.5, 300.5, 302)
    r_bins = np.linspace(-100.5, 100.5, 102)
    plt.subplot(211)
    plt.hist(ls, t_bins, color='g', alpha=0.5)
    plt.hist(ps, t_bins, color='b', alpha=0.5)
    plt.subplot(212)
    plt.hist(fe, r_bins, color='r')
    plt.tight_layout()
    plt.savefig('Results/hist_%s.png' % dataset)
    plt.close()

    with open('Results/%s.csv' % dataset, 'w') as fp:
        fp.write(stats_str)


def store_results_2(dataset, data, values, name=None):
    errors = {}
    ls, ps = [], []
    for i in range(len(data)):
        speed = values[i]
        switch = data[i][3]
        position = data[i][4]
        ref_speed = data[i][2]
        err = speed - ref_speed
        rel = speed / ref_speed - 1.0
        ls.append(ref_speed)
        ps.append(speed)

        key = '%s+%s' % (switch, position)
        if key not in errors:
            errors[key] = []
        errors[key].append([err, rel])

    stats_str = ''
    fe, fr = [], []
    keys = sorted(errors)
    for key in keys:
        an = key.split('+')[0]
        pn = key.split('+')[1]
        e = [v[0] for v in errors[key]]
        r = [v[1] for v in errors[key]]
        fe = list(fe) + list(e)
        fr = list(fr) + list(r)
        std = float(np.std(e))
        mbe = float(np.mean(e))
        mae = float(np.mean(np.abs(e)))
        rle = np.mean(np.abs(r)) * 100.0
        e10 = np.mean([1.0 if abs(ri) >= 0.1 else 0.0 for ri in r]) * 100.0
        sss = '%16s - %20s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%' % (an, pn, mae, mbe, std, rle, e10)
        print(sss)
        stats_str += sss + '\n'
    std = float(np.std(fe))
    mbe = float(np.mean(fe))
    mae = float(np.mean(np.abs(fe)))
    rle = np.mean(np.abs(fr)) * 100.0
    e10 = np.mean([1.0 if abs(ri) >= 0.1 else 0.0 for ri in fr]) * 100.0
    print('%39s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%' % ('Overall', mae, mbe, std, rle, e10))
    stats_str += '%39s: MAE = %6.2f, MBE = %6.2f, STD = %6.2f, MRE = %5.1f%%, E10 = %5.1f%%\n' % ('Overall', mae, mbe, std, rle, e10)

    if name is None:
        plt_file = 'Results/hist_%s.png' % dataset
    else:
        plt_file = 'Results/hist_%s-%s.png' % (name, dataset)

    t_bins = np.linspace(-0.5, 300.5, 302)
    r_bins = np.linspace(-100.5, 100.5, 102)
    plt.subplot(211)
    plt.hist(ls, t_bins, color='g', alpha=0.5)
    plt.hist(ps, t_bins, color='b', alpha=0.5)
    plt.subplot(212)
    plt.hist(fe, r_bins, color='r')
    plt.tight_layout()
    plt.savefig(plt_file)
    plt.close()

    if name is None:
        filename = 'Results/%s.csv' % dataset
    else:
        filename = 'Results/%s-%s.csv' % (name, dataset)

    with open(filename, 'w') as fp:
        fp.write(stats_str)


def load_validation_data(dataset, **kwargs):
    input_length = kwargs.pop('input_length', 1024)
    model_length = kwargs.pop('model_length', None)
    feature = kwargs.pop('feature_name', 'wnd_psd')
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    if cache:
        cache_file = ''
    else:
        cache_file = ''

    # Get file paths and speed labels from selected source
    dataset = dataset.lower()
    if dataset == 'ocvia':
        rows = load_ocvia()
    elif dataset == 'ocvia_alt':
        rows = load_ocvia_alt()
    elif dataset == 'gen_speed':
        rows = load_gen_speed()
    elif dataset == 'sats_data':
        rows = load_sats()
    else:
        raise ValueError

    # if debug is active only a tiny subset of the data will be used to quicker test later stages of the pipeline
    if debug:
        np.random.shuffle(rows)
        rows = rows[:200]

    # Extract relevant features from the data and assemble them into an input and target array
    x_train, y_train = [], []
    for row in rows:
        x_file, speed = row
        x = utils.load_binary(x_file)

        # Extract Features
        speed /= 10.0
        x_train, y_train = extract_feature(x, speed, x_train, y_train, input_length, model_length, feature)
    x_train = np.array(x_train).reshape((-1, input_length, 1))
    y_train = np.array(y_train)

    # Cache processed data for quicker re-use later
    if cache:
        utils.save_binary('Cache/ocvia_cache_x.dat', x_train.reshape((-1)))
        utils.save_binary('Cache/ocvia_cache_y.dat', y_train)
    return x_train, y_train


def load_cv(dataset, **kwargs):
    datasets = ['gen_db', 'gen_jre', 'gen_ocvia', 'gen_sncf', 'gen_tvk', 'gen_bn']
    if dataset not in datasets:
        exit(-1)

    x = []
    datasets.remove(dataset)
    for d in datasets:
        x = list(x) + list(load_training_data(d, **kwargs))
    v = load_training_data(dataset, **kwargs)
    return x, v


def load_ocvia_data(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    if cache and os.path.isfile('Cache/ocvia_cache_x.dat'):
        x_train = utils.load_binary('Cache/ocvia_cache_x.dat').reshape((-1, input_length, 1))
        y_train = utils.load_binary('Cache/ocvia_cache_y.dat')
        return x_train, y_train

    dataset_path = 'D:/Data/20200518 - Ocvia Speed Phase 2/20200518 - Ocvia Speed Phase 2'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    x_train, y_train = [], []
    count = 0

    if debug:
        np.random.shuffle(labels)
        labels = labels[:1000]

    for label in labels:
        count += 1
        utils.command_progress(count, len(labels), post_fix='Loading Some Data')

        x_file = utils.get_files('%s/data' % dataset_path, '%s*' % label[0][:24])
        if len(x_file) < 1:
            continue
        x_file = x_file[0]
        if not os.path.isfile(x_file):
            continue
        spd = float(label[1]) / 10.0
        x = utils.load_binary(x_file)

        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)

        # x_train.append(extract_feature(x))
        # y_train.append(spd)
        # for i in range(0, len(x) - input_length * 2, 256):
        #     psd = extract_feature(x[i:i + input_length * 2])
        #     x_train.append(psd)
        #     y_train.append(spd)
    input_length = 256  # TODO: Debug
    x_train = np.array(x_train).reshape((-1, input_length, 1))
    y_train = np.array(y_train)

    if cache:
        utils.save_binary('Cache/ocvia_cache_x.dat', x_train.reshape((-1)))
        utils.save_binary('Cache/ocvia_cache_y.dat', y_train)

    return x_train, y_train


def load_alt_ocvia_data(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    if cache and os.path.isfile('Cache/alt_ocvia_cache_x.dat'):
        x_train = utils.load_binary('Cache/alt_ocvia_cache_x.dat').reshape((-1, input_length, 1))
        y_train = utils.load_binary('Cache/alt_ocvia_cache_y.dat')
        return x_train, y_train

    dataset_path = 'D:/Data/20200131_Ocvia_Synth'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    x_train, y_train = [], []
    count = 0

    if debug:
        np.random.shuffle(labels)
        labels = labels[:1000]

    for label in labels:
        count += 1
        utils.command_progress(count, len(labels), post_fix='Loading Some Data')

        x_file = utils.get_files('%s/data' % dataset_path, '%s*' % label[0][:24])
        if len(x_file) < 1:
            continue
        x_file = x_file[0]
        if not os.path.isfile(x_file):
            continue
        spd = float(label[6]) / 10.0
        x = utils.load_binary(x_file)[400:-4000]
        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)

    input_length = 256  # TODO: Debug
    x_train = np.array(x_train).reshape((-1, input_length, 1))
    y_train = np.array(y_train)

    if cache:
        utils.save_binary('Cache/alt_ocvia_cache_x.dat', x_train.reshape((-1)))
        utils.save_binary('Cache/alt_ocvia_cache_y.dat', y_train)

    return x_train, y_train


def load_gen_speed_data(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    model_length = kwargs.pop('model_length', 256)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    if cache and os.path.isfile('Cache/alt_ocvia_cache_x.dat'):
        x_train = utils.load_binary('Cache/gen_speed_cache_x.dat').reshape((-1, input_length, 1))
        y_train = utils.load_binary('Cache/gen_speed_cache_y.dat')
        return x_train, y_train

    dataset_path = 'D:/Data/Speed_Truth_v01'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]
    x_train, y_train = [], []
    count = 0

    if debug:
        np.random.shuffle(labels)
        labels = labels[:1000]

    for label in labels:
        count += 1
        utils.command_progress(count, len(labels), post_fix='Loading Some Data')

        x_file = utils.get_files('%s/data' % dataset_path, '%s*' % label[0][:24])
        if len(x_file) < 1:
            continue
        x_file = x_file[0]
        if not os.path.isfile(x_file):
            continue
        spd = float(label[8]) / 10.0
        x = utils.load_binary(x_file)[400:-4000]
        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)

    x_train = np.array(x_train).reshape((-1, model_length, 1))
    y_train = np.array(y_train)

    if cache:
        utils.save_binary('Cache/gen_speed_cache_x.dat', x_train.reshape((-1)))
        utils.save_binary('Cache/gen_speed_cache_y.dat', y_train)

    return x_train, y_train


def load_sats_speed(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    if cache and os.path.isfile('Cache/sats_cache_x.dat'):
        x_train = utils.load_binary('Cache/sats_cache_x.dat').reshape((-1, input_length, 1))
        y_train = utils.load_binary('Cache/sats_cache_y.dat')
        return x_train, y_train

    dataset_path = 'D:/Data/SATS Dataset'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]

    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]

    x_train, y_train = [], []
    for label in labels:
        x_file = '%s/data/%s' % (dataset_path, label[0])
        spd = float(label[4]) / 10.0
        x = utils.load_binary(x_file)[400:-4000]
        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)

        # for i in range(0, len(x) - input_length * 2, 256):
        #     psd = extract_feature(x[i:i + input_length * 2])
        #     x_train.append(psd)
        #     y_train.append(spd)
    input_length = 256  # TODO: Debug
    x_train = np.array(x_train).reshape((-1, input_length, 1))
    y_train = np.array(y_train)

    if cache:
        utils.save_binary('Cache/sats_cache_x.dat', x_train.reshape((-1)))
        utils.save_binary('Cache/sats_cache_y.dat', y_train)

    return x_train, y_train


def load_sats_validation(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    dataset_path = 'D:/Data/SATS Dataset'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]

    if debug:
        np.random.shuffle(labels)
        labels = labels[:1000]

    data = []
    for label in labels:
        x_file = '%s/data/%s' % (dataset_path, label[0])
        spd = float(label[4]) / 10.0
        x = utils.load_binary(x_file)[400:-4000]

        x_train, y_train = [], []
        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)
        # for i in range(0, len(x) - input_length * 2, 256):
        #     psd = extract_feature(x[i:i + input_length * 2])
        #     x_train.append(psd)
        #     y_train.append(spd)
        x_train = np.array(x_train).reshape((-1, input_length, 1))
        y_train = np.array(y_train)
        data.append([x, x_train, y_train])
    return data


def load_ocvia_validation(**kwargs):
    input_length = kwargs.pop('input_length', 1024)
    debug = kwargs.pop('debug', False)
    cache = kwargs.pop('cache', False)

    dataset_path = 'D:/Data/20200131_Ocvia_Synth'
    with open('%s/labels.csv' % dataset_path, 'r') as fp:
        labels = [line.replace('\n', '').split(',') for line in fp.readlines()[1:]]

    if debug:
        np.random.shuffle(labels)
        labels = labels[:100]

    data = []
    for label in labels:
        x_file = '%s/data/%s' % (dataset_path, label[0])
        spd = float(label[6]) / 10.0
        x = utils.load_binary(x_file)[400:-4000]

        x_train, y_train = [], []
        x_train, y_train = extract_feature(x, spd, x_train, y_train, input_length)

        # x_train.append(extract_feature(x))
        # y_train.append(spd)

        # for i in range(0, len(x) - input_length * 2, 256):
        #     psd = extract_feature(x[i:i + input_length * 2])
        #     x_train.append(psd)
        #     y_train.append(spd)
        x_train = np.array(x_train).reshape((-1, 256, 1))
        # x_train = np.array(x_train).reshape((-1, input_length, 1))
        y_train = np.array(y_train)
        data.append([x, x_train, y_train])
    return data


def filter_train_category(data, category):
    a = [row for row in data if row[5] != category]
    b = [row for row in data if row[5] == category]
    return a, b


def filter_measurement_location(data, c=0.5):
    keys = list(set(['%s+%s' % (row[2], row[3]) for row in data]))
    np.random.shuffle(keys)
    n = int(float(len(keys)) * c)
    keys_a = keys[:n]
    keys_b = keys[n:]
    a = [row for row in data if '%s+%s' % (row[2], row[3]) in keys_a]
    b = [row for row in data if '%s+%s' % (row[2], row[3]) in keys_b]
    return a, b


def filter_tiny_category(data, category):
    keys = list(set(['%s+%s' % (row[2], row[3]) for row in data]))
    zh = {key: [row for row in data if '%s+%s' % (row[2], row[3]) == key and row[5] == 'HIGH SPEED'] for key in keys}
    zp = {key: [row for row in data if '%s+%s' % (row[2], row[3]) == key and row[5] == 'PASSENGER'] for key in keys}
    zc = {key: [row for row in data if '%s+%s' % (row[2], row[3]) == key and row[5] == 'CARGO'] for key in keys}
    a, b = [], []
    for key in keys:
        np.random.shuffle(zh[key])
        np.random.shuffle(zp[key])
        np.random.shuffle(zc[key])
        if category in ['ALL', 'FULL']:
            a = a + zh[key][:10] + zp[key][:10] + zc[key][:10]
            b = b + zh[key][10:] + zp[key][10:] + zc[key][10:]
        elif category == 'HIGH SPEED':
            a = a + zh[key][:10]
            b = b + zh[key][10:]
        elif category == 'PASSENGER':
            a = a + zp[key][:10]
            b = b + zp[key][10:]
        elif category == 'CARGO':
            a = a + zc[key][:10]
            b = b + zc[key][10:]
    return a, b


# Add some filters
def load_ahe_dataset(**kwargs):
    path = 'D:/Data/20200713 - General Speed PSD'
    dataset = kwargs.pop('dataset', 'Infrabel')
    ttcls = kwargs.pop('category', None)
    mode = kwargs.pop('mode', 'random')

    df_a = pd.read_parquet('%s/%s.parquet' % (path, dataset))
    df_b = pd.read_parquet('%s/%s_speed_dataset.parquet' % (path, dataset))
    df_b = df_b.drop_duplicates(subset='filename')
    df_b = pd.merge(left=df_a, right=df_b, how='left', on='filename')
    psds = np.load('%s/%s.psd_250_2s.npy' % (path, dataset))
    psds = np.sqrt(psds) * 100.0

    if dataset in ['DB', 'JRE', 'SNCF', 'Ocvia', 'TVK']:
        spd = df_b['speed'].values
        sws = df_b['asset_name'].values
        pss = df_b['measurement_location'].values
        tts = df_b['locomotive'].values
        cls = df_b['category'].values
        data = [[x, y, sw, ps, tt, cl] for x, y, sw, ps, tt, cl in zip(psds, spd, sws, pss, tts, cls)]
    elif dataset in ['BN']:
        spd = df_b['corrected_speed'].values
        sws = df_b['asset'].values
        pss = df_b['measurement_location'].values
        # tts = df_b['train_type'].values  # Because why the fuck would we keep the train type...
        ttn = 'PASSENGER'
        cls = 'PASSENGER'
        data = [[x, y, sw, ps, ttn, cls] for x, y, sw, ps in zip(psds, spd, sws, pss)]
    elif dataset in ['Infrabel']:
        nope = ['FALSE WAKEUP', 'MULTIPLE WAKEUP']
        sws = [f.split('/')[4] for f in df_b['filename'].values]
        pss = [f.split('/')[5] for f in df_b['filename'].values]
        tts = [ttype for ttype in df_b['pred_frog']]
        cls = ['UNKNOWN' if tt in nope else 'CARGO' if tt == 'CARGO' else 'PASSENGER' for tt in tts]
        spd = df_b['speed_frog'].values
        data = [[x, y, sw, ps, tt, cl] for x, y, sw, ps, tt, cl in zip(psds, spd, sws, pss, tts, cls)]
    else:
        raise ValueError

    if mode == 'random':
        np.random.shuffle(data)
        training = data[:len(data) // 2]
        validate = data[len(data) // 2:]
    elif mode == 'category':
        training, validate = filter_train_category(data, category=ttcls)
    elif mode == 'location':
        training, validate = filter_measurement_location(data, c=0.5000)
    elif mode == 'tiny_category':
        training, validate = filter_tiny_category(data, category=ttcls)
    elif mode == 'training':
        training = data
        validate = []
    elif mode == 'validate':
        training = []
        validate = data

    x_train = np.array([v[0] for v in training]).reshape((-1, 250, 1))
    y_train = np.array([v[1] for v in training]).repeat(10)
    x_valid = np.array([v[0] for v in validate]).reshape((-1, 250, 1))
    y_valid = np.array([v[1] for v in validate]).repeat(10)
    x_tests = np.array([v[0] for v in validate]).reshape((-1, 10, 250, 1))
    y_tests = np.array([v[1] for v in validate])
    z_tests = [[v[2], v[3], v[4], v[5]] for v in validate]
    training = [[0, x, min(max(y / 160.0 - 1.0, -1.0), 1.0)] for x, y in zip(x_train, y_train)]
    validate = [[0, x, min(max(y / 160.0 - 1.0, -1.0), 1.0)] for x, y in zip(x_valid, y_valid)]
    test_set = [[0, x, y, z[0], z[1], z[2], z[3]] for x, y, z in zip(x_tests, y_tests, z_tests)]
    return training, validate, test_set


def cross_cargo(dataset):
    sets = ['DB', 'Infrabel', 'JRE', 'Ocvia', 'TVK']
    x, y, z = load_ahe_dataset(dataset=dataset, mode='category', category='CARGO')
    sets.remove(dataset)
    for i in range(len(sets)):
        t, _, _ = load_ahe_dataset(dataset=sets[i], mode='training')
        x += t
    return x, y, z


if __name__ == '__main__':
    pass

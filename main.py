import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


from keras.layers import Input, Dense, Conv1D, Activation, BatchNormalization, Dropout, Concatenate, AveragePooling1D
from keras.layers import Flatten
from keras.models import Model, model_from_json
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.optimizers import Adam
from keras.regularizers import l2
from kxnn import build, standard
import matplotlib.pyplot as plt
import keras.backend as K
import numpy as np
import pathlib
import pickle
import data


def quantile_loss(q, y, f):
    e = (y-f)
    return K.mean(K.maximum(q*e, (q-1)*e), axis=-1)


def save_model(model_name, model):
    json_string = model.to_json()
    json_file = model_name + '.json'
    hdf5_file = model_name + '.h5'
    open(json_file, 'w').write(json_string)
    model.save_weights(hdf5_file, overwrite=True)


def load_model(model_name):
    json_file = model_name + '.json'
    hdf5_file = model_name + '.h5'
    model = model_from_json(open(json_file).read())
    model.load_weights(hdf5_file)
    return model


def scheduler(epoch):
    if epoch < 250:
        return 1e-3
    else:
        return 5e-4


# Dense Conv Net
def dense_conv_model(**kwargs):
    regularize = kwargs.pop('regularize', False)

    if regularize:
        def dense_block(x_in, filters=6, filter_len=65):
            x = BatchNormalization()(x_in)
            x = Activation('relu')(x)
            x = Conv1D(filters, filter_len, padding='same', kernel_regularizer=l2(2e-5))(x)
            x = Dropout(0.1)(x)
            return x

        input_length = kwargs.pop('wnd_size', 1024)
        input_layer = Input((input_length, 1))
        y1 = Conv1D(16, 9, padding='same', kernel_regularizer=l2(2e-5))(input_layer)
        y2 = dense_block(y1, filters=12, filter_len=9)
        xt = Concatenate()([y1, y2])
        y3 = dense_block(xt, filters=12, filter_len=9)
        xt = Concatenate()([y1, y2])
        xt = Concatenate()([xt, y3])
        y4 = dense_block(xt, filters=40, filter_len=9)
        y4 = AveragePooling1D(2)(y4)
        y5 = dense_block(y4, filters=12, filter_len=9)
        xt = Concatenate()([y4, y5])
        y6 = dense_block(xt, filters=12, filter_len=9)
        xt = Concatenate()([y4, y5])
        xt = Concatenate()([xt, y6])
        y7 = BatchNormalization()(xt)
        y7 = Activation('relu')(y7)
        y7 = AveragePooling1D(8)(y7)
        y7 = Flatten()(y7)
        y7 = Dense(256, kernel_regularizer=l2(2e-5))(y7)
        output_layer = Dense(1, activation='tanh', kernel_regularizer=l2(2e-5))(y7)
        model = Model(input_layer, output_layer)
        # model.compile(optimizer=Adam(lr=1e-4), loss='mse', metrics=['mae'])
        model.compile(optimizer=Adam(lr=1e-3), loss='logcosh', metrics=['mae'])
    else:
        def dense_block(x_in, filters=6, filter_len=65):
            x = BatchNormalization()(x_in)
            x = Activation('relu')(x)
            x = Conv1D(filters, filter_len, padding='same')(x)
            x = Dropout(0.1)(x)
            return x

        input_length = kwargs.pop('wnd_size', 1024)
        input_layer = Input((input_length, 1))
        y1 = Conv1D(16, 9, padding='same')(input_layer)
        y2 = dense_block(y1, filters=12, filter_len=9)
        xt = Concatenate()([y1, y2])
        y3 = dense_block(xt, filters=12, filter_len=9)
        xt = Concatenate()([y1, y2])
        xt = Concatenate()([xt, y3])
        y4 = dense_block(xt, filters=40, filter_len=9)
        y4 = AveragePooling1D(2)(y4)
        y5 = dense_block(y4, filters=12, filter_len=9)
        xt = Concatenate()([y4, y5])
        y6 = dense_block(xt, filters=12, filter_len=9)
        xt = Concatenate()([y4, y5])
        xt = Concatenate()([xt, y6])
        y7 = BatchNormalization()(xt)
        y7 = Activation('relu')(y7)
        y7 = AveragePooling1D(8)(y7)
        y7 = Flatten()(y7)
        y7 = Dense(256)(y7)
        output_layer = Dense(1, activation='tanh')(y7)
        model = Model(input_layer, output_layer)
        # model.compile(optimizer=Adam(lr=1e-3), loss='logcosh', metrics=['mae'])
        model.compile(optimizer=Adam(lr=1e-3), loss='mae', metrics=['mae'])

    return model


# Dense Conv Net with Quantile Regression Loss
def dense_conv_model_quantile(**kwargs):
    def dense_block(x_in, filters=6, filter_len=65):
        x = BatchNormalization()(x_in)
        x = Activation('relu')(x)
        x = Conv1D(filters, filter_len, padding='same')(x)
        x = Dropout(0.1)(x)
        return x

    input_length = kwargs.pop('wnd_size', 1024)
    input_layer = Input((input_length, 1))
    y1 = Conv1D(16, 9, padding='same')(input_layer)
    y2 = dense_block(y1, filters=12, filter_len=9)
    xt = Concatenate()([y1, y2])
    y3 = dense_block(xt, filters=12, filter_len=9)
    xt = Concatenate()([y1, y2])
    xt = Concatenate()([xt, y3])
    y4 = dense_block(xt, filters=40, filter_len=9)
    y4 = AveragePooling1D(2)(y4)
    y5 = dense_block(y4, filters=12, filter_len=9)
    xt = Concatenate()([y4, y5])
    y6 = dense_block(xt, filters=12, filter_len=9)
    xt = Concatenate()([y4, y5])
    xt = Concatenate()([xt, y6])
    y7 = BatchNormalization()(xt)
    y7 = Activation('relu')(y7)
    y7 = AveragePooling1D(8)(y7)
    y7 = Flatten()(y7)
    y7 = Dense(256)(y7)
    # output_layer = Dense(1, activation='tanh')(y7)
    out_10 = Dense(1, activation='tanh', name='quantile_10')(y7)
    out_50 = Dense(1, activation='tanh', name='quantile_50')(y7)
    out_90 = Dense(1, activation='tanh', name='quantile_90')(y7)

    model = Model(input_layer, [out_10, out_50, out_90])
    model.compile(
        optimizer=Adam(lr=1e-3),
        loss={
            'quantile_10': lambda y, f: quantile_loss(0.1, y, f),
            'quantile_50': lambda y, f: quantile_loss(0.5, y, f),
            'quantile_90': lambda y, f: quantile_loss(0.9, y, f)
        },
        metrics=['mae']
    )
    return model


def fit_model(x, y, model, u=None, v=None, **kwargs):
    model_name = kwargs.pop('model_name', '')
    plt_name = kwargs.pop('plt_name', None)
    epochs = kwargs.pop('epochs', 100)
    show = kwargs.pop('show', True)

    filepath = "Models/%s.h5" % model_name
    checkpoint = ModelCheckpoint(filepath, monitor='val_mae', verbose=1, save_best_only=True, mode='min')
    schedule = LearningRateScheduler(scheduler)
    callbacks_list = [checkpoint, schedule]

    if u is None:
        logger = model.fit(x, y, epochs=epochs, batch_size=64, callbacks=callbacks_list)
    else:
        log_t = model.fit(x, y, epochs=250, batch_size=64, validation_data=[u, v], callbacks=callbacks_list)
        model.load_weights(filepath)
        logger = model.fit(x, y, epochs=500, batch_size=64, validation_data=[u, v], callbacks=callbacks_list, initial_epoch=250)
        logger.history['loss'] = log_t.history['loss'] + logger.history['loss']
        logger.history['val_loss'] = log_t.history['val_loss'] + logger.history['val_loss']
    open('Models/%s.json' % model_name, 'w').write(model.to_json())
    model.load_weights(filepath)

    leg_str = []
    if 'loss' in logger.history:
        leg_str.append('loss')
        plt.plot(logger.history['loss'])
    if 'val_loss' in logger.history:
        leg_str.append('val_loss')
        plt.plot(logger.history['val_loss'])
    plt.legend(leg_str)
    plt.tight_layout()
    if show:
        plt.show()
    elif plt_name is not None:
        plt.savefig('Results/%s-%s.png' % (model_name, plt_name))
        plt.close()
    else:
        plt.close()


def visual_model_validation(x, model):
    k = 0
    results = []
    rel_err = []
    for xi in x:
        k += 1
        p = model.predict(xi[1])
        p = (np.median(p) + 1.0) * 160.0
        v = (np.median(xi[2]) + 1.0) * 160.0
        results.append(p - v)
        rel_err.append(np.abs(p / v - 1) * 100.0)
    rel_err = np.array(rel_err)
    ten_err = (float(len(rel_err[rel_err > 10.0])) / float(len(rel_err))) * 100.0
    print('MAE: %.1f km/h' % np.mean(np.abs(results)))
    print('MAX: %.1f km/h' % np.max(np.abs(results)))
    print('10%%: %.1f %%' % ten_err)

    k = 0
    for xi in x:
        k += 1
        p = model.predict(xi[1])

        leg = []
        plt.plot(xi[0])
        for i in range(len(p)):
            plt.plot(0, 0)
            pv = (np.median(p) + 1.0) * 160.0
            xv = (np.median(xi[2]) + 1.0) * 160.0
            leg.append('%5.1f vs %5.1f' % (pv, xv))
        plt.ylim(np.min(xi[0][500:]) * 1.10, 1.10 * np.max(xi[0][500:]))
        plt.legend(leg, loc='upper right')
        plt.tight_layout()
        plt.show()


def train_model():
    caching = True
    testing = False
    input_length = 1024
    x, y = data.load_ocvia_data(input_length=input_length, debug=testing, cache=caching)
    u, v = data.load_sats_speed(input_length=input_length, debug=testing, cache=caching)
    model = dense_conv_model(wnd_size=input_length)
    fit_model(x, y, model, u, v)


def experiment_final_cross(**kwargs):
    save_plot = kwargs.pop('save_plot', False)
    dataset = kwargs.pop('dataset', 'gen_sncf')
    experiment_base = kwargs.pop('experiment', 'in-client')

    testing = False
    input_length = 1024
    model_length = 256

    cache_data = data.load_binned_cache(dataset=dataset)
    for index in range(5):
        experiment = '%s-%d' % (experiment_base, index)
        options = {
            'input_length': input_length,
            'model_length': model_length,
            'feature_name': 'welch_psd',
            'model_name': 'dense_conv_fc1_%s-%s' % (experiment, dataset),
            'epochs': 500,
            'debug': testing,
            'plt_name': dataset if save_plot else None,
            'show': False if save_plot else True
        }
        xy, uv, wz = data.load_bin_index(cache_data, index)
        model = dense_conv_model(wnd_size=model_length)
        if dataset == 'gen_ocvia':
            model.load_weights('Models/dense_conv_base_db.h5')
        else:
            model.load_weights('Models/dense_conv_base_ocvia.h5')

        x = np.array([value[1] for value in xy])
        y = np.array([value[2] for value in xy])
        u = np.array([value[1] for value in uv])
        v = np.array([value[2] for value in uv])
        fit_model(x, y, model, u, v, **options)

        values = []
        for xi in wz:
            p = model.predict(xi[1])
            p = (np.median(p) + 1.0) * 160.0
            values.append(p)
        data.store_results_2(dataset, wz, values, name=experiment)


def experiment_final(**kwargs):
    save_plot = kwargs.pop('save_plot', False)
    dataset = kwargs.pop('dataset', 'gen_sncf')
    experiment = kwargs.pop('experiment', 'in-client')

    testing = False
    input_length = 1024
    model_length = 256

    options = {
        'input_length': input_length,
        'model_length': model_length,
        'feature_name': 'welch_psd',
        'model_name': 'dense_conv_f4_%s-%s' % (experiment, dataset),
        'epochs': 500,
        'debug': testing,
        'plt_name': dataset if save_plot else None,
        'show': False if save_plot else True
    }

    xy, uv, wz = data.load_cache(dataset=dataset)
    model = dense_conv_model(wnd_size=model_length)
    if dataset == 'gen_ocvia':
        model.load_weights('Models/dense_conv_base_db.h5')
    else:
        model.load_weights('Models/dense_conv_base_ocvia.h5')

    x = np.array([value[1] for value in xy])
    y = np.array([value[2] for value in xy])
    u = np.array([value[1] for value in uv])
    v = np.array([value[2] for value in uv])
    fit_model(x, y, model, u, v, **options)

    values = []
    for xi in wz:
        p = model.predict(xi[1])
        p = (np.median(p) + 1.0) * 160.0
        values.append(p)
    data.store_results_2(dataset, wz, values, name=experiment)


def final_experiment():
    # experiment_final(save_plot=True, dataset='gen_sncf')
    # experiment_final(save_plot=True, dataset='gen_db')
    # experiment_final(save_plot=True, dataset='gen_bn')
    # experiment_final(save_plot=True, dataset='gen_fgc')
    # experiment_final(save_plot=True, dataset='gen_jre')
    # experiment_final(save_plot=True, dataset='gen_ocvia')
    experiment_final(save_plot=True, dataset='gen_tvk')


def read_files(flags):
    scale = flags.scale
    x_train_dir = pathlib.Path(f"{standard.INPUTS_DIR}") / "x_train"
    y_train_dir = pathlib.Path(f"{standard.INPUTS_DIR}") / "y_train"
    x_train = None
    y_train = None
    for x_train_file in x_train_dir.iterdir():
        y_train_file = y_train_dir / x_train_file.name.replace("train_x", "train_y")
        if x_train_file.suffix == ".gz":
            x_train_file = x_train_file.with_suffix("")
        if y_train_file.suffix == ".gz":
            y_train_file = y_train_file.with_suffix("")
        x_train_ = standard.load_file(str(x_train_file))
        y_train_ = standard.load_file(str(y_train_file))
        if scale in standard.scalers_dict.keys():
            scaler = standard.scalers_dict[scale](x_train_)
            x_train_ = scaler.transform(x_train_)
            with open(f"{standard.OUTPUTS_DIR}{x_train_file.name[:-15]}_{scale}_scaler.pickle", "wb") as file:
                pickle.dump(scaler, file)
        if x_train is None:
            x_train = x_train_
            y_train = y_train_
        else:
            x_train = np.concatenate((x_train, x_train_), axis=0)
            y_train = np.concatenate((y_train, y_train_), axis=0)
    return x_train, y_train


def train_procedure():
    parser = standard.get_parser()
    parser.add_argument('--cv', type=int, default=0)
    flags = parser.parse_args()

    client = flags.client
    x_train, y_train = read_files(flags)


def test_kx_load():
    model_path = 'Speed Fold Models/DB-2X-0-v001.h5'
    model = build.load_model(model_path)
    pass


if __name__ == '__main__':
    test_kx_load()
    # final_experiment()
    # experiment_final_cross(dataset='gen_bn', save_plot=True)
